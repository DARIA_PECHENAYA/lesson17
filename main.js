/*самовызывающаяся функция - i независимые друг от друга*/

// (function () {
//     let i = ;
// })();

// const b = 7;
// function parent(a) {
//     console.log(this);
//     return function (b) {
//         console.log(this);
//         return a + b
//     }
// }
// console.log(this);
// const fun1 = parent(7);
// console.log(fun1(1));

// function parent(...arg) {
//     return function (sum) {
//         arg.forEach((count) =>{
//             sum+=count;
//         });
//         console.log(sum);
//     }
// }
// const sum = parent(1, 2, 3);
// console.log(sum(0));

/*Написать с использованием замыканий функцию,
которая будет хранить и складывать принимаемые
аргументы в сумму и возвращать ее.*/

// function parent(...arg) {
//     const sum = function () {
//         let result = 0;
//         arg.forEach((count) => {
//             result += count;
//         });
//         return result;
//     };
//     return sum();
// }
// const sum = parent(1, 2, 3);
// console.log(sum);

/*Создать кэш данных в объекте и возвращать данные из
кэша, если значение имеется или записывать в него
новые значения, если не имеется.*/

// const obj = {
//     banner: 'Hello',
//     link: 'google.com'
// };
// function creatCache(obj) {
//     if(obj.cache){
//         return obj.cache;
//     }
//     obj.cache = Object.keys(obj).join();
// }
// console.log(creatCache(obj));
// console.log(creatCache(obj));

/*Дополнить объект методом, который возвращает
количество всех его ключей(1)*/

// const obj = {
//     banner: 'Hello',
//     link: 'google.com'
// };
// obj.method = function () {
//    return Object.keys(this).length;
// };
// console.log(obj.method());

/*Дополнить объект методом, который возвращает
количество всех его ключей(2)*/

// const obj = {
//     banner: 'Hello',
//     link: 'google.com'
// };
// obj.method = function () {
//     let result = 0;
//     Object.keys(this).forEach((key) => {
//         if (typeof this[key] !== 'function') {
//             result++;
//         }
//     });
//     return result;
// }
// console.log(obj.method());

/*toString*/

// const obj = {
//         banner: 'Hi ',
//         link: 'google.com '
// };
// console.log('test ' + obj);
// obj.toString = function(){
//     return this.banner + this.link;
// };
// console.log('test ' + obj);

/*valueOf*/

// const obj = {
//     banner: 'Hi ',
//     link: 'google.com '
// };
// console.log(7 + obj);
// obj.valueOf = function(){
//     return Object.keys(this).length;
// };
// console.log( 7 + obj);

/*Создать объект машины car, который имеет свойство "передача" и
метод, который повышает/понижает передачу на 1. Также у объекта
должен быть метод, который в зависимости от передачи
возвращает сообщение о скорости: (-1) - 'back', 0 - 'neutral', 1 - 'slow',
2 - 'medium', 3 - 'fast', 4 - 'very fast'. При попытке сложить объект car с
числом - число должно складываться в свойство speed и speed
должна возвращаться. Если скорость больше 5 - должно быть
выведено сообщение "Warning! Your speed is too high!"*/

// const car = {
//     gear: 0,
//     changeGera: function(isUp){
//         if(isUp){
//             this.gear++;
//         } else{
//             this.gear--;
//         }
//     },
//     getGearInfo: function(){
//         let result;
//         switch (thisGear) {
//             case -1:
//                 result ='back';
//                 break;
//             case 0:
//                 result ='neutral';
//                 break;
//             case 1:
//                 result ='slow';
//                 break;
//             case 2:
//                 result ='medium';
//                 break;
//             case 3:
//                 result ='fast';
//                 break;
//             case 4:
//                 result ='very fast';
//                 break;
//         }
//         return result;
//     },
//     speed: 0,
//     valueOf: function(a){
//         this.speed = this.speed ;
//         return this.speed;
//     }
// }
// car.speed = 4 + car.valueOf();
// console.log(car.speed);
// if(car.speed >= 5) {
//     alert('warning!');
// }

// const arr = ['test', 'test2'];
// const [v1, v2, v3] = arr;
// console.log(v1);
// console.log(v2);
// console.log(v3);

// const obj = {
//     namel:'123',
//     email:'hdchchj.com',
//     age:124
// };
// const {name, email, age} = obj;
// console.log(namel);
// console.log(email);
// console.log(age);

// const obj = {
//     namel:'123',
//     email:'hdchchj.com',
//     age:124,
//     address: {
//         street: 'PUB',
//         houseNumbeer: 12
//     }
// };
// const { address: {street} } = obj;
// console.log(street);

// const obj = {
//     namel:'123',
//     email:'hdchchj.com',
//     age:124,
//     address: {
//         street: 'PUB',
//         houseNumbeer: 12
//     }
// };
// const { address: {street, apportament = 15, houseNumbeer = 6 } } = obj;
// console.log(street);
// console.log(apportament);
// console.log(obj);

/*Деструктуризация и spread-оператор*/

// const obj = {
//     namel:'123',
//     email:'hdchchj.com',
//     age:124,
//     address: {
//         street: 'PUB',
//         houseNumbeer: 12
//     }
// };
// const { address: {street: test } } = obj;
// console.log(test);
// console.log(obj);

// function test(...arg) {
//     console.log(arg);
// }
// test(2,54,6,'string');

/*Деструктуризация и spread-оператор*/

// const user1 = {
//     namel:'Alex',
//     lastName:undefined,
//     age: 15
// };
// const user2 = {
//     ...user1,
//     namel:'Ivan',
//     lastName:'coub',
// };
// console.log(user1);
// console.log(user2);

/*Деструктуризация и spread-оператор*/

// const user1 = {
//     namel:'Alex',
//     lastName:'test',
//     age: 15
// };
// const user2 = {
//     ...user1,
//     namel:'Ivan',
//     lastName: undefined,
    
// };
// console.log(Object.assign({}, user1, user2));
// console.log(user1);
// console.log(user2);
